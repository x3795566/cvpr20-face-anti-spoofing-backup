from utils import *
import random
from collections import OrderedDict
from pathlib import Path

DATA_ROOT = os.environ['DATA_ROOT']
RESIZE_SIZE = int(os.environ['RESIZE_SIZE'])

proto_map = {
    '4@1': {
        'train': '4@1_train.txt',
        'val': '4@1_dev_ref.txt',
        'test': '4@1_test_res.txt'
    },
    '4@2': {
        'train': '4@2_train.txt',
        'val': '4@2_dev_ref.txt',
        'test': '4@2_test_res.txt'
    },
    '4@3': {
        'train': '4@3_train.txt',
        'val': '4@3_dev_ref.txt',
        'test': '4@3_test_res.txt'
    }
}


def load_train_videos(proto):
    dict = OrderedDict()
    f = open(DATA_ROOT + proto_map[proto]['train'])
    lines = f.readlines()
    img_num = len(lines)

    for line in lines:
        path, label = line.strip().split(' ')
        id = os.path.join(*path.split('/')[-4:-2])
        path = DATA_ROOT + path

        if id in dict:
            dict[id][0].append(path)
        else:
            dict[id] = [[path], label]
    list = []
    for key, value in dict.items():
        list.append([value[0], value[1], key])

    return list, img_num


def load_train_list(proto):
    list = []
    f = open(DATA_ROOT + proto_map[proto]['train'])
    lines = f.readlines()

    for line in lines:
        line = line.strip().split(' ')
        list.append(line)
    return list


def load_val_videos(proto):
    dict = OrderedDict()
    f = open(DATA_ROOT + proto_map[proto]['val'])
    lines = f.readlines()
    img_num = 0

    for line in lines:
        id, label = line.strip().split(' ')
        frames_dir = os.path.join(DATA_ROOT, id, 'profile')
        paths = list(Path(frames_dir).rglob("*.jpg"))
        img_num += len(paths)
        paths.sort()
        for path in paths:
            path = str(path)
            if id in dict:
                dict[id][0].append(path)
            else:
                dict[id] = [[path], label]

    vlist = []
    for key, value in dict.items():
        vlist.append([value[0], value[1], key])

    return vlist, img_num


def load_val_list(proto):
    vlist = []
    f = open(DATA_ROOT + proto_map[proto]['val'])
    lines = f.readlines()

    for line in lines:
        line = line.strip().split(' ')

        frames_dir = os.path.join(DATA_ROOT, line[0], 'profile')
        frames = list(Path(frames_dir).rglob("*.jpg"))
        for frame in frames:
            vlist.append([os.path.join(line[0], 'profile', os.path.basename(str(frame))), line[1]])
    return vlist


def load_val_id(proto):
    ids = []
    f = open(DATA_ROOT + proto_map[proto]['val'])
    lines = f.readlines()

    for line in lines:
        line = line.strip().split(' ')
        ids.append(line[0])
    return ids


def load_test_videos(proto):
    dict = OrderedDict()
    f = open(DATA_ROOT + proto_map[proto]['test'])
    lines = f.readlines()

    for line in lines:
        id = line.strip()
        frames_dir = os.path.join(DATA_ROOT, id, 'profile')
        paths = list(Path(frames_dir).rglob("*.jpg"))
        paths.sort()
        for path in paths:
            path = str(path)
            if id in dict:
                dict[id][0].append(path)
            else:
                dict[id] = [[path], -1]

    tlist = []
    for key, value in dict.items():
        tlist.append([value[0], value[1], key])

    return tlist


def load_test_list(proto):
    from pathlib import Path
    tlist = []
    f = open(DATA_ROOT + proto_map[proto]['test'])
    lines = f.readlines()

    for line in lines:
        line = line.strip()

        frames_dir = os.path.join(DATA_ROOT, line, 'profile')
        frames = list(Path(frames_dir).rglob("*.jpg"))
        for frame in frames:
            tlist.append([os.path.join(line, 'profile', os.path.basename(str(frame)))])

    return tlist


def load_test_id(proto):
    ids = []
    f = open(DATA_ROOT + proto_map[proto]['test'])
    lines = f.readlines()

    for line in lines:
        line = line.strip().split(' ')
        ids.append(line[0])
    return ids

def load_test_id(proto):
    list = []
    f = open(DATA_ROOT + proto_map[proto]['test'])
    lines = f.readlines()

    for line in lines:
        line = line.strip()
        list.append(line)
    return list


def transform_balance(train_list):
    print('balance!!!!!!!!')
    pos_list = []
    neg_list = []
    for tmp in train_list:
        if tmp[1]=='1':
            pos_list.append(tmp)
        else:
            neg_list.append(tmp)

    print(len(pos_list))
    print(len(neg_list))
    return [pos_list, neg_list]


def submission(val_probs, test_probs, outname, proto, video_mode=False):
    fout = open(outname, 'w')

    if video_mode:
        for probs, path_key in [[val_probs, 'val'], [test_probs, 'test']]:
            if path_key == 'val':
                ids = load_val_id(proto)
            else:
                ids = load_test_id(proto)

            for prob, id in zip(probs, ids):
                out = id + ' ' + str(prob)
                fout.write(out + '\n')
    else:
        for probs, path_key in [[val_probs, 'val'], [test_probs, 'test']]:
            if path_key == 'val':
                paths = load_val_list(proto)
            else:
                paths = load_test_list(proto)
            vote = {}
            ids = []
            for prob, path in zip(probs, paths):
                path = path[0]
                id = os.path.join(*path.split('/')[-4:-2])
                if id not in vote:
                    vote[id] = [prob]
                    ids.append(id)
                else:
                    vote[id].append(prob)

            for id in ids:
                out = id + ' ' + str(sum(vote[id])/len(vote[id]))
                fout.write(out+'\n')

    fout.close()


if __name__ == '__main__':
    load_train_videos('4@1')
