# Code Submission v2 by LeoHou
## Recent Update

**`2020.03.02`**: add 0301 submission

**`2020.02.29`**: add fusion model for 0229 submission

**`2020.02.25`**: code upload for the organizers to reproduce.

#### Prerequisite
* ##### Expected dataset directory structure
    ```
    /path/to/cvpr20/dataset/
        |─  4@1_train.txt
        |─  4@2_train.txt
        |─  4@3_train.txt
        |─  4@1_dev_ref.txt
        |─  4@2_dev_ref.txt
        |─  4@3_dev_ref.txt
        |─  4@1_test_res.txt
        |─  4@2_test_res.txt
        |─  4@3_test_res.txt
        |─  train/
        |─  dev/
        └─  test/
    ```
* ##### Install python packages
    ```
    pip3 install -r requirements.txt
    ```    
* ##### Set DATA_ROOT environment variable
    ```shell script
    export DATA_ROOT=/path/to/cvpr20/dataset/
    ```
  
<br>

#### Train models

* ##### Train local feature models
    Run training script using the following commands to train models for each sub-protocol. \
    
    ```shell script
    ./cnn-train
    ```
    The results will be generate at <b>models_4@1/, models_4@2/, models_4@3/</b> folders
    
* ##### Train view range fusion models
    ```shell script
    ./fusion-cnn-train
    ```
    The results will be generate at <b>models_4@1/, models_4@2/, models_4@3/</b> folders

* ##### Train new dynamic feature models
    Prerequisite: local feature models must be trained first for rnn models to load cnn models as part of pretrained weight
    ```shell script
    ./rnn-train
    ```
    The results will be generate at <b>rnn_models_4@1/, rnn_models_4@2/, rnn_models_4@3/</b> folders

* ##### Train old dynamic feature models
    This script is coded for 3 GPUs environment. If your GPU less than 3, please modify CUDA_VISIBLE_DEVICES variables in the script. 
    ```shell script
    git checkout 9540caa6
    ./cvpr-train-toufen
    ```
    The results will be generate at <b>rnn_models_4@1/, rnn_models_4@2/, rnn_models_4@3/</b> folders


<br>

#### Infer validation(dev) and test set using pretrained models
```shell script
# for local feature models
./cnn-test

# for dynamic feature models
./rnn-test

# for view range fusion models
./fusion-cnn-test
```
The results will be generate at the corresponding folders.\
e.g.
>1. cnn_models_4@1/model_A_color_48/checkpoint/global_test_36_TTA/global_min_acer_model.pth_noTTA.txt
>2. rnn_models_4@1/params_4_1_48/checkpoint/global_test_36_TTA/global_min_loss_model.pth.txt
>3. cnn_models_4@1/model_A_fusion_48/checkpoint/global_test_36_TTA/global_min_loss_model.pth.txt

<br><br>


#### For origanizer team to reproduce the final submissions on 03/01
* ##### Preparation

    unzip the 0301-models.tar.gz in the project root folder
    
    ```shell script
    tar zxvf 0301-models.tar.gz
    ``` 

* ##### Infer and ensemble all models prediction results

    ```shell script
    # Set dataset path
    export DATA_ROOT=/path/to/cvpr20/dataset/
    
    # Checkout to old version code for generating the first rnn model's predictions
    git checkout rnn-models-v1
    chmod +x ./cvpr-test-toufen
    ./cvpr-test-toufen
    
    # Go back to newest version code
    git checkout master 
    
    # Run the following scripts for generating other three preditions
    chmod +x ./*-test
    ./cnn-test
    ./fusion-cnn-test
    ./rnn-test
    
    # Merge all predictions
    python3 submission.py
    ```
    The ensemble results are generate at 0301_super_4@1.txt, 0301_super_4@2.txt, 0301_super_4@3.txt
    Then, concatenate all results to get the final submission in phase2:
    ```
    cat 0301_super_4@*.txt >> 0301_final_submisssion.txt
    ```








