from imutils.face_utils import FaceAligner
from imutils.face_utils import rect_to_bb
from process.data_helper import load_train_list
from process.data_helper import DATA_ROOT
import random
import dlib
import cv2
import os


detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('./process/shape_predictor_68_face_landmarks.dat')
fa = FaceAligner(predictor, desiredFaceWidth=224, desiredLeftEye=(0.23, 0.23))
list = load_train_list('4@3')

while True:
    path = list[random.randint(0, len(list)-1)][0]
    path = os.path.join(DATA_ROOT, path)
    image = cv2.imread(path)

    cv2.imshow("Input", image)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    rects = detector(image, 2)

    # loop over the face detections
    for rect in rects:
        # extract the ROI of the *original* face, then align the face
        # using facial landmarks
        (x, y, w, h) = rect_to_bb(rect)
        x = max(0, x)
        y = max(0, y)
        faceOrig = image[y:y + h, x:x + w]
        faceAligned = fa.align(image, gray, rect)
        # faceAligned = faceAligned[y:y + h, x:x + w]

        # display the output images
        cv2.imshow("Original", faceOrig)
        cv2.imshow("Aligned", faceAligned)
        cv2.waitKey()

cv2.destroyAllWindows()
