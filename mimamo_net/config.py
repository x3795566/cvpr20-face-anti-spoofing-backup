import argparse

parser = argparse.ArgumentParser(description="two stream rnn")
parser.add_argument('--proto', type=str, default='4@1')
parser.add_argument('--mode', type=str, default='train', choices=['train', 'infer_test'])

# ========================= Model Configs ==========================
parser.add_argument('--hidden_units', default=[2048, 256, 256], type=int, nargs="+", help='hidden units set up')
parser.add_argument('--seqlen', type=int, default=5)
parser.add_argument('--phase_num', type=int, default=2, help='the number of input phase difference images')
parser.add_argument('--phase_size', type=int, default=48)
parser.add_argument('--image_size', type=int, default=48)
parser.add_argument('--checkpoint_name', type=str, default=r'global_min_acer_model.pth')
parser.add_argument('--block_phase', action='store_true')

# ========================= Learning Configs ==========================
parser.add_argument('--epochs', default=25, type=int, metavar='N', help='number of total epochs to run')
parser.add_argument('-b', '--batch_size', default=128, type=int, metavar='N', help='mini-batch size')
parser.add_argument('--lr', default=1e-3, type=float)
parser.add_argument('--weight-decay', '--wd', default=5e-4, type=float, metavar='W', help='weight decay')
parser.add_argument('--cycle_num', type=int, default=10)
parser.add_argument('--cycle_inter', type=int, default=50)
