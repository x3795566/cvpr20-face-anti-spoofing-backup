from utils import *
from process.augmentation import *
from process.data_helper import *
from process.prepro import cut_black_edge

class FDDataset(Dataset):
    def __init__(self, mode, proto, modality='color', fold_index='<NIL>', image_size=48, balance=True):
        super(FDDataset, self).__init__()
        print('fold: '+str(fold_index))
        print(modality)

        self.augmentor = FaceFeaturesAugmentor(mode != 'train')
        self.mode = mode
        self.modality = modality
        self.balance = balance

        self.channels = 3
        self.image_size = image_size
        self.fold_index = fold_index

        self.set_mode(self.mode, self.fold_index, proto)

    def set_mode(self, mode, fold_index, proto):
        self.mode = mode
        self.fold_index = fold_index
        print(proto)
        print('fold index set: ', fold_index)
        print('set dataset mode: ', mode)

        if self.mode == 'test':
            self.test_list = load_test_list(proto)
            self.num_data = len(self.test_list)

        elif self.mode == 'val':
            self.val_list = load_val_list(proto)
            self.num_data = len(self.val_list)

        elif self.mode == 'train':
            self.train_list = load_train_list(proto)
            random.shuffle(self.train_list)
            self.num_data = len(self.train_list)

            if self.balance:
                self.train_list = transform_balance(self.train_list)

        print(self.num_data)

    def __getitem__(self, index):

        if self.fold_index is None:
            print('WRONG!!!!!!! fold index is NONE!!!!!!!!!!!!!!!!!')
            return

        if self.mode == 'train':
            if self.balance:
                if random.randint(0, 1) == 0:
                    tmp_list = self.train_list[0]
                else:
                    tmp_list = self.train_list[1]

                pos = random.randint(0, len(tmp_list) - 1)
                color, label = tmp_list[pos]
            else:
                color, label = self.train_list[index]

        elif self.mode == 'val':
            color, label = self.val_list[index]

        elif self.mode == 'test':
            color = self.test_list[index][0]
            test_id = color

        color = cv2.imread(os.path.join(DATA_ROOT, color), 1)
        color = cut_black_edge(color)
        color = cv2.resize(color, (RESIZE_SIZE, RESIZE_SIZE))

        blocks = []
        for shape in [(48, 48, 3), (64, 64, 3), (96, 96, 3)]:
            block = self.augmentor.exec(color, target_shape=shape)
            blocks.append(block)

        if self.mode == 'train':
            norm_blocks = []
            for block in blocks:
                block = cv2.resize(block, (48, 48))
                norm_blocks.append(block)

            image = np.concatenate(norm_blocks, axis=2)

            if random.randint(0, 1) == 0:
                random_pos = random.randint(0, 2)
                if random.randint(0, 1) == 0:
                    image[:, :, 3 * random_pos:3 * (random_pos + 1)] = 0
                else:
                    for i in range(3):
                        if i != random_pos:
                            image[:, :, 3 * i:3 * (i + 1)] = 0

            image = np.transpose(image, (2, 0, 1))
            image = image.astype(np.float32)
            image = image.reshape([self.channels * 3, self.image_size, self.image_size])
            image = image / 255.0

            label = int(label)
            return torch.FloatTensor(image), torch.LongTensor(np.asarray(label).reshape([-1]))

        elif self.mode == 'val':
            n = len(blocks[0])

            norm_blocks = []
            for TTA_blocks in blocks:
                norm_TTA_blocks = []
                for TTA_block in TTA_blocks:
                    shape = TTA_block.shape
                    TTA_block = TTA_block.reshape(shape[1], shape[2], shape[3])
                    TTA_block = cv2.resize(TTA_block, (48, 48))
                    TTA_block = TTA_block.reshape(1, 48, 48, 3)
                    norm_TTA_blocks.append(TTA_block)
                norm_TTA_blocks = np.concatenate(norm_TTA_blocks, axis=0)
                norm_blocks.append(norm_TTA_blocks)

            image = np.concatenate(norm_blocks, axis=3)

            image = np.transpose(image, (0, 3, 1, 2))
            image = image.astype(np.float32)
            image = image.reshape([n, self.channels * 3, self.image_size, self.image_size])
            image = image / 255.0

            label = int(label)
            return torch.FloatTensor(image), torch.LongTensor(np.asarray(label).reshape([-1]))


        elif self.mode == 'test':
            n = len(blocks[0])

            norm_blocks = []
            for TTA_blocks in blocks:
                norm_TTA_blocks = []
                for TTA_block in TTA_blocks:
                    shape = TTA_block.shape
                    TTA_block = TTA_block.reshape(shape[1], shape[2], shape[3])
                    TTA_block = cv2.resize(TTA_block, (48, 48))
                    TTA_block = TTA_block.reshape(1, 48, 48, 3)
                    norm_TTA_blocks.append(TTA_block)
                norm_TTA_blocks = np.concatenate(norm_TTA_blocks, axis=0)
                norm_blocks.append(norm_TTA_blocks)

            image = np.concatenate(norm_blocks, axis=3)

            image = np.transpose(image, (0, 3, 1, 2))
            image = image.astype(np.float32)
            image = image.reshape([n, self.channels * 3, self.image_size, self.image_size])
            image = image / 255.0
            return torch.FloatTensor(image), test_id

    def __len__(self):
        return self.num_data


# check #################################################################
def run_check_train_data():
    dataset = FDDataset(mode='val', proto='4@1', image_size=48)
    print(dataset)
    # special cases 4947 'train/1_103_3_1_4/profile/0028.jpg'
    idx = random.randint(0, len(dataset)-1)
    image, label = dataset[idx]
    print('pause')


# main #################################################################
if __name__ == '__main__':
    print( '%s: calling main function ... ' % os.path.basename(__file__))
    run_check_train_data()


