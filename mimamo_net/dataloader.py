import torch.utils.data as data
from mimamo_net.transforms import *
from mimamo_net.subutils import Steerable_Pyramid_Phase
from process.data_helper import *
from process.prepro import cut_black_edge
import cv2
import torch
from process.augmentation import color_augumentor
from process.augmentation import general_augmentor
from process.augmentation import random_cropping
from process.augmentation import deterministic_random_cropping
from process.prepro import FaceAligner
import math

FRAME_RESIZE_SIZE = int(112 + 112*0.15)
CROP_SIZE = 112


def phase_2_output(phase_batch, steerable_pyramid, return_phase=False):
    """
    phase_batch dim: bs, num_phase, W, H
    """
    sp = steerable_pyramid
    num_frames, num_phases, W, H = phase_batch.size()
    coeff_batch = sp.build_pyramid(phase_batch)
    assert isinstance(coeff_batch, list)
    phase_batch_0 = sp.extract_phase(coeff_batch[0], return_phase=return_phase)
    num_frames, n_ch, n_ph, W, H = phase_batch_0.size()
    phase_batch_0 = phase_batch_0.view(num_frames, -1, W, H)
    phase_batch_1 = sp.extract_phase(coeff_batch[1], return_phase=return_phase)
    num_frames, n_ch, n_ph, W, H = phase_batch_1.size()
    phase_batch_1 = phase_batch_1.view(num_frames, -1, W, H)
    return phase_batch_0, phase_batch_1


class FaceFramesDataset(data.Dataset):
    def __init__(self, mode, proto, block_size,
                 py_level=4, py_nbands=2, phase_num=2, phase_size=48, seqlen=7, return_phase=False, block_phase=False,
                 balance=True):
        self.seqlen = seqlen  # length of sequence as input to the RNN
        self.phase_num = phase_num
        self.phase_size = phase_size
        self.return_phase = return_phase
        # self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.steerable_pyramid = Steerable_Pyramid_Phase(height=py_level, nbands=py_nbands, scale_factor=2,
                                                         device=torch.device("cpu"),
                                                         extract_level=[1, 2], visualize=False)
        self.balance = balance
        self.set_mode(mode, proto)
        self.augment = color_augumentor
        self.block_size = block_size
        self.aligner = FaceAligner(CROP_SIZE)
        self.block_phase = block_phase

    def set_mode(self, mode, proto):
        self.mode = mode

        if self.mode == 'test':
            self.test_list = load_test_videos(proto)
            self.num_data = len(self.test_list)
            print('set dataset mode: test')

        elif self.mode == 'val':
            self.val_list, _ = load_val_videos(proto)
            self.num_data = len(self.val_list)
            print('set dataset mode: val')

        elif self.mode == 'train':
            self.train_list, frames_num = load_train_videos(proto)
            random.shuffle(self.train_list)
            self.num_data = frames_num//self.seqlen

            if self.balance:
                self.train_list = transform_balance(self.train_list)
            print('set dataset mode: train')

        print(self.num_data)

    def __len__(self):
        return self.num_data

    def prepro(self, image, image_size=64, channels=3, start_x=0, start_y=0):
        image = cv2.resize(image, (RESIZE_SIZE, RESIZE_SIZE))

        if self.mode == 'train':
            raw_image = deterministic_random_cropping(image, (image_size, image_size, 3), is_random=True,
                                                      start_x=start_x, start_y=start_y)

            image = cv2.resize(raw_image, (image_size, image_size))
            image = np.transpose(image, (2, 0, 1))
            image = image.astype(np.float32)
            image = image.reshape([channels, image_size, image_size])
            image = image / 255.0

        elif self.mode == 'val':
            # image = self.augment(image, target_shape=(image_size, image_size, 3), is_infer=True)
            raw_image = deterministic_random_cropping(image, (image_size, image_size, 3), is_random=True,
                                                      start_x=start_x, start_y=start_y)
            image = [raw_image.reshape([1, image_size, image_size, 3])]
            n = len(image)
            image = np.concatenate(image, axis=0)
            image = np.transpose(image, (0, 3, 1, 2))
            image = image.astype(np.float32)
            image = image.reshape([n, channels, image_size, image_size])
            image = image / 255.0

        elif self.mode == 'test':
            # raw_image = self.augment(image, target_shape=(image_size, image_size, 3), is_infer=True)
            raw_image = deterministic_random_cropping(image, (image_size, image_size, 3), is_random=True,
                                                      start_x=start_x, start_y=start_y)
            image = [raw_image.reshape([1, image_size, image_size, 3])]

            n = len(image)
            image = np.concatenate(image, axis=0)
            image = np.transpose(image, (0, 3, 1, 2))
            image = image.astype(np.float32)
            image = image.reshape([n, channels, image_size, image_size])
            image = image / 255.0
        else:
            raise Exception('Unknown mode')

        return image, raw_image

    @staticmethod
    def to_same_ratio(imgs):
        max_w, max_h = 0, 0
        for img in imgs:
            w, h, c = img.shape
            if w > max_w:
                max_w = w
            if h > max_h:
                max_h = h
        result = []
        for img in imgs:
            shape = img.shape
            newimg = np.zeros((max_w, max_h, 3), np.uint8)
            ax, ay = (max_h - img.shape[1]) // 2, (max_w - img.shape[0]) // 2
            newimg[ay:ay + shape[0], ax:ax + shape[1]] = img
            newimg = cv2.resize(newimg, (FRAME_RESIZE_SIZE, FRAME_RESIZE_SIZE))
            result.append(newimg)

        return result

    def get_data(self, seq, label):
        raw_imgseq = []
        for path in seq:
            img = cv2.imread(path, cv2.IMREAD_COLOR)
            img = cut_black_edge(img)
            img = self.aligner.align(img)
            raw_imgseq.append(img)

        # imgseq = self.to_same_ratio(raw_imgseq)
        if self.mode == 'train':
            # do general augmentation
            aug_imgseq = general_augmentor(raw_imgseq, crop_shape=(CROP_SIZE, CROP_SIZE, 3))
        else:
            aug_imgseq = general_augmentor(raw_imgseq, crop_shape=(CROP_SIZE, CROP_SIZE, 3), is_infer=True)

        # Pick sub-sequence for generating phase images
        subseqs = []
        for fid in range(1, len(aug_imgseq)-1):
            phase_range = []
            for i in range(self.phase_num + 1):
                step = i - self.phase_num // 2
                idx = max(0, fid + step)
                idx = min(idx, len(aug_imgseq) - 1)
                phase_range.append(idx)
            subseqs.append(phase_range)

        img_subseqs = [[aug_imgseq[idx] for idx in subseq] for subseq in subseqs]

        # get a random block from the center image of image sequence
        cenblocks = []
        block_subseqs = []
        start_x = random.randint(0, RESIZE_SIZE - self.block_size)
        start_y = random.randint(0, RESIZE_SIZE - self.block_size)
        cenidx = (self.phase_num + 1) // 2
        for img_subseq in img_subseqs:
            block_subseq = []
            for i, img in enumerate(img_subseq):
                block, raw_block = self.prepro(img, image_size=self.block_size, start_x=start_x, start_y=start_y)
                if i == cenidx:
                    cenblocks.append(block)
                block_subseq.append(raw_block)
            block_subseqs.append(block_subseq)

        if self.block_phase:
            phase_batch_0, phase_batch_1 = self.to_phase(block_subseqs, self.mode)
        else:
            phase_batch_0, phase_batch_1 = self.to_phase(img_subseqs, self.mode)
        # img_subseqs = np.array(img_subseqs)
        # return [raw_imgseq, cenblocks, img_subseqs[:, (self.phase_num+1)//2], img_subseqs, phase_batch_0, phase_batch_1,
        #         torch.LongTensor(np.asarray(label).reshape([-1]))]
        return [torch.FloatTensor(np.array(cenblocks)),
                phase_batch_0, phase_batch_1,
                torch.LongTensor(np.asarray(label).reshape([-1]))]

    def to_phase(self, seqs, mode):
        # transform rgb to grayscale for phase extraction
        flat_phase_images = []
        if mode == 'TTA_test':
            for seq in seqs:
                for TTA_imgs in seq:
                    for TTA_img in TTA_imgs:
                        TTA_img = TTA_img.reshape(TTA_img.shape[1], TTA_img.shape[2], TTA_img.shape[3])
                        gray = cv2.cvtColor(TTA_img, cv2.COLOR_BGR2GRAY)
                        flat_phase_images.append(gray)
        else:
            for seq in seqs:
                for img in seq:
                    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                    flat_phase_images.append(gray)

        phase_transform = torchvision.transforms.Compose([GroupToPILImage(),
                                                          GroupScale(size=self.phase_size),
                                                          Stack(),
                                                          ToTorchFormatTensor()])
        flat_phase_images = phase_transform(flat_phase_images)
        if mode == 'TTA_test':
            phase_augimg = flat_phase_images.view(len(seqs)*len(seqs[0][0]), self.phase_num + 1, self.phase_size, self.phase_size)
        else:
            phase_augimg = flat_phase_images.view(len(seqs), self.phase_num + 1, self.phase_size, self.phase_size)
        phase_augimg = torch.FloatTensor(phase_augimg)
        phase_batch_0, phase_batch_1 = phase_2_output(phase_augimg, self.steerable_pyramid,
                                                      return_phase=self.return_phase)
        if mode == 'TTA_test':
            phase_batch_0 = phase_batch_0.view(len(seqs), len(seqs[0][0]), phase_batch_0.shape[1], phase_batch_0.shape[2], phase_batch_0.shape[3])
            phase_batch_1 = phase_batch_1.view(len(seqs), len(seqs[0][0]), phase_batch_1.shape[1],
                                               phase_batch_1.shape[2], phase_batch_1.shape[3])

        return phase_batch_0, phase_batch_1

    def __getitem__(self, index):

        if self.mode == 'train':
            if self.balance:
                if random.randint(0, 1) == 0:
                    tmp_list = self.train_list[0]
                else:
                    tmp_list = self.train_list[1]

                pos = random.randint(0, len(tmp_list) - 1)
                frames, label, id = tmp_list[pos]
            else:
                frames, label, id = self.train_list[index]

        elif self.mode == 'val':
            frames, label, id = self.val_list[index]

        elif self.mode == 'test':
            frames, label, id = self.test_list[index]
        else:
            raise Exception('Unknown mode')

        # if len(frames) < self.seqlen:
        #     raise Exception('Too less frames: ' + str(len(frames)))
        if len(frames) < self.seqlen:
            gennum = math.ceil(len(frames) / self.seqlen)
            frames = frames * gennum
            print('Warning: too less frames')

        label = int(label)

        if self.mode == 'train':
            start = random.randint(0, len(frames) - self.seqlen)
            end = start + self.seqlen
            seq = frames[start:end]
            out = self.get_data(seq, label)
            return out
        elif self.mode == 'val':
            blocks_list = []
            phase0_batch_list = []
            phase1_batch_list = []
            label_list = []
            for i in range(2*len(frames)//self.seqlen):
                start = random.randint(0, len(frames) - self.seqlen)
                end = start + self.seqlen
                seq = frames[start:end]
                blocks, phase0_batch, phase1_batch, label = self.get_data(seq, label)
                blocks_list.append(blocks)
                phase0_batch_list.append(phase0_batch)
                phase1_batch_list.append(phase1_batch)
                label_list.append(label)

            return torch.stack(blocks_list), torch.stack(phase0_batch_list), torch.stack(phase1_batch_list), \
                   label
        else:
            blocks_list = []
            phase0_batch_list = []
            phase1_batch_list = []
            label_list = []

            # num = int(math.ceil(len(frames)/self.seqlen))
            # starts = random.sample(range(upper+1), num)
            # upper = len(frames) - self.seqlen
            # threshold = 2*len(frames)//self.seqlen
            # if upper > threshold:
            # if True:
            # starts = random.sample(range(upper + 1), 2*len(frames)//self.seqlen)
            # else:
            #     starts = range(upper + 1)

            for i in range(2*len(frames)//self.seqlen):
                start = random.randint(0, len(frames) - self.seqlen)
                end = start + self.seqlen
                seq = frames[start:end]
                blocks, phase0_batch, phase1_batch, label = self.get_data(seq, label)
                blocks_list.append(blocks)
                phase0_batch_list.append(phase0_batch)
                phase1_batch_list.append(phase1_batch)
                label_list.append(label)

            return torch.stack(blocks_list), torch.stack(phase0_batch_list), torch.stack(phase1_batch_list), \
                   label


if __name__ == '__main__':
    # from train_MIMAMO import do_valid
    # from train_MIMAMO import softmax_cross_entropy_criterion as criterion
    # from network import TwoStreamRNN
    dataset = FaceFramesDataset(mode='test', proto='4@1', block_size=48, block_phase=True, phase_num=2)
    # val_loader = DataLoader(dataset, shuffle=False, batch_size=2, drop_last=False, num_workers=4, pin_memory=False)
    # do_valid(TwoStreamRNN([2048, 256, 256], num_phase=2), val_loader, criterion, torch.device('cpu'))
    raw_imgseq, blocks, center_augimgs, phase_augimgs, phase0_imgs, phase1_imgs, label = dataset[0]
    # blocks, phase0_imgs, phase1_imgs, label = dataset[0]
    bad_imgseq = []
    for rawimg in raw_imgseq:
        # badimg = cv2.resize(rawimg, (FRAME_RESIZE_SIZE, FRAME_RESIZE_SIZE))
        bad_imgseq.append(rawimg)
    cv2.imshow('bad scaling', np.hstack(bad_imgseq))
    for block, center_augimg, augimg, phase0, phase1 in zip(blocks, center_augimgs, phase_augimgs, phase0_imgs,
                                                            phase1_imgs):
        # cv2.imshow('phase img', np.hstack(phaseimg))
        cv2.imshow('block', np.array(block))
        cv2.imshow('center img', np.array(center_augimg))
        cv2.imshow('transformed phase', np.hstack(augimg))
        cv2.imshow('phase0', np.hstack(phase0))
        cv2.imshow('phase1', np.hstack(phase1))
        cv2.waitKey()

    cv2.destroyAllWindows()
    print('done!')
